# Activity:
# 1. Create an abstract class called Animal that has the following abstract methods:
# - eat(food)
# - make_sound()
# 2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:
# - Properties:
# 	- Name
# 	- Breed
# 	- Age
# - Methods:
# 	- Getters
# 	- Setters
# 	- Implementation of abstract methods
# 	- call()

from abc import ABC, abstractclassmethod

class Animal(ABC):

    @abstractclassmethod
    def eat(self, food):
        pass

    @abstractclassmethod
    def make_sound(self):
        pass

class Dog(Animal):

    def __init__(self, name, breed, age):
        super().__init__()
        self.name = name
        self.breed = breed
        self.age = age

    def set_name(self, name):
        self.name = name

    def set_breed(self, breed):
        self.breed = breed

    def set_age(self, age):
        self.age = age

    def get_name(self):
        print(f"This dog's name is {self.name}")

    def get_breed(self):
        print(f"This dog's breed is {self.breed}")

    def get_age(self):
        print(f"This dog's age is {self.age}")

    def eat(self, food):
        print(f"Eaten {food}")
    
    def make_sound(self):
        print("Bark! Woof! Arf!")

    def call(self):
        print(f"Here {self.name}!")

class Cat(Animal):

    def __init__(self, name, breed, age):
        super().__init__()
        self.name = name
        self.breed = breed
        self.age = age

    def set_name(self, name):
        self.name = name

    def set_breed(self, breed):
        self.breed = breed

    def set_age(self, age):
        self.age = age

    def get_name(self):
        print(f"This cat's name is {self.name}")

    def get_breed(self):
        print(f"This cat's breed is {self.breed}")

    def get_age(self):
        print(f"This cat's age is {self.age}")

    def eat(self, food):
        print(f"Serve me {food}")
    
    def make_sound(self):
        print("Miaow! Nyaw! Nyaaaaa!")

    def call(self):
        print(f"{self.name}, come on!")




# Test Cases:
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()
